# Branches

#### Switch branches

```sh
git checkout <name>
```

#### Create and switch branch

```sh
git checkout -b <name>
```

#### Delete branch

```sh
git branch -d <name>
```

#### Avoid merge when you pull

```sh
git pull -r
```

#### Git ignore - remove files from remote repository

```sh
git rm -r --cached <filenameInRepo>
```

#### Git stash - save changes when you switch branch

```sh
git stash
git stash pop - get the changes back
```

#### Undoing 2 commits

```sh
git reset --hard HEAD~2
git reset --soft HEAD~2 - it keeps the changes in your working dir

# undoing commits in remote repo
git reset --hard HEAD~2
git push --force

# create a new commit with the previous changes, avoiding to delete branch
git rebert <commit-code>
git push
```
