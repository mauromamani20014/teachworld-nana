# Commit changes

```sh
# Commit a new image, installed locally
docker commit -m "mensaje" -a "author" <idcontainer> <dockerhub-name>/<name-new-container>:<tag>

# Create image with a new tag
docker image tag <repository>/<image>:<tag> <newRepository>/<new-image>:<newTag>

# Push images to docker hub
docker login
docker image push <image-name>

```
