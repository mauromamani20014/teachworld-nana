# Pulling Images

```sh
# Pull an image
docker image pull <image>:<tag>

# Run a container image
docker container run <imagename>

# Stop a container image
docker container stop <imagename | id | name>

# Run a container image and the port will be a random port
docker container run -P <imagename>

# Start a container
docker container start <id>

# Getting shell in container
docker container run -it centos

# Lab: running web server
docker container run -d -p 8080:80 --name mysite nginx
# -d: iniciar el proceso en segundo plano como demon
# -p: 8080:80: usará el puerto 8080 para exponer en LAN o internet, pero internamente docker usara el 80
# --name: le damos un nombre al container
```
