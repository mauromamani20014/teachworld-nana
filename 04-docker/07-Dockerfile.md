# Dockerfile

```sh
# Build a docker image with Dockerfile
docker image build -t <name>:<tag> .
.: build in this directory

# Persisting data, create volume
docker volume create <name>

# See info of volume
docker volume inspect <name>

# Delete volume
docker volume rm <name>

# Run container in volume
docker container run -d -p 80:80 --name=mywebapp -v <volume:name>:<path> <image:nginx>


```
