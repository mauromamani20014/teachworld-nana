# Basic docker

```sh
# Add docker repository - documentation

# Install
sudo apt install docker-ce docker-ce-cli containerd.io

# Append group to user
sudo usermod -aG docker <username>

# See running containers
docker container ls

# See all containers
docker container ls -a

# List availables images
docker image ls

# Run a image, if is not available in the machine, it will search and install it
docker container run hello-world

#
#To generate this message, Docker took the following steps:
# 1. The Docker client contacted the Docker daemon.
# 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
#    (amd64)
# 3. The Docker daemon created a new container from that image which runs the
#    executable that produces the output you are currently reading.
# 4. The Docker daemon streamed that output to the Docker client, which sent it
#    to your terminal.
#

```
