# Getting information about container

```sh
# See port
docker container port <name | id>

# See logs
docker container logs <name | id>
-f: realtime

# See information
docker container inspect <name | id>

```
