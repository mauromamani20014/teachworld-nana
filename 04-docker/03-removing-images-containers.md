# Removing images and containers


```sh
# Stop container
docker container stop <id>

# Remove container
docker container rm <id>

# List exited containers
docker container ls -a -f status=exited

# List id of exited containers
docker container ls -a -f status=exited -q

# Remove images
docker image rm <imagename>

```
