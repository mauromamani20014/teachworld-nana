# Scripts

#### sharp-bang

```bash
#!/bin/bash
```


#### sample

```bash
#!/bin/bash

echo "Setup and configure server"
```

#### Variables

```bash
#!/bin/bash

file_name=config.yaml

# Store output of a command in a variable
config_files=$(ls config)

echo "using file $file_name to configure"
echo "here are all configuration files: $config_files"
```

#### Conditionals

```bash
#!/bin/bash

if [-d "config"];then
  echo "config is a directory!"
else
  echo "config is not a directory"
  echo "creating one..."
  mkdir config
fi
```

#### Basics operators

```
-d: if file is directory
-f: if file is ordinary file
-r: if file is readable
-w: if file is writable
-x: if file is executable
-s: if file size is greater than 0
-e: if file exists
-u: check userID
-g: check groupID

-eq: are equals       ==
-gt: greater than     >
-ge: greater or equal >=
-lt: less than        <
-le: less or equal    <=
-ne: not equal        !=

$?: capture value returned by the last command
```

#### Parameters

```
#!/bin/bash

$variable1=$1
$variable2=$2

if ["$variable1" -eq "$variable2"]; then
  echo "They have the same value!"
else 
  echo "they are different values"
fi

# count parameters
$*: represent all the arguments as a single string
$#: numbers of params
```

#### Read user input

```
#!/bin/bash

echo "Reading user input"

read -p "Please enter your password: " <variable-name>

echo "Thank. Your password is $<variable-name>"
```

#### Loops

```
#!/bin/bash

# For loop
for param in $*; do
  echo $param 
done

# While loop
while true; do
echo "Executing to infinity..."
done
```

#### Functions

```
#!/bin/bash

function score_sum {
  echo "This is a function"
}
// call function
score_sum

// parameters
function welcome() {
  name=$1
  echo "Hi, $name"
}
welcome nana
```
