# User Permissions

Each file has 2 levels of owners
1) user
2) groups

## Change user-group owner
```
sudo chown <username>:<groupname> <filename>
```

## Change userowner
```
sudo chown <username> <filename>
```

## Change group owner
```
sudo chgrp <groupname> <filename>
```

## Change permissions
```
Owner: u | Group: g | Other: o
Add: u+r | Remove: u-w | Set: u=w--
Read: r/4 | Write: w/2 | Execute: x/1

sudo chmod <permission> <filename>
```
