# SYSTEMD

```sh
# Start service
systemctl start <name.service> || <name>

# Check status of a service
systemctl status <name.service> || <name>

# Stop service
systemctl stop <name.service> || <name>

# Restart service
systemctl restart <name.service> || <name>

# Enable a service to start at boot time
systemctl enable <name.service> || <name>

# Disable a service to start at boot time
systemctl disable <name.service> || <name>

# check if a service start at boot time
systemctl is-enabled <name.service> || <name>
```
