# ANACRON
hecho para maquinas que no estan encendidas todo el tiempo

#### Create task in anacron

```sh
# Root user

vim /etc/anacrontab

# check errors in anacron
anacron -T

# check tasks 
sudo anacron -d
```
