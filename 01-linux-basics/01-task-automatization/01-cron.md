# CRON: 

crontab no funcionará si se reinicia el servidor, esta hecho para un servidor continuo
#### Edit crontab

```sh
# create crontab
crontab -e

# remove crontab
crontab -r

# see crontab file
crontab -l

# open crontab of specific user as root
crontab -r -u <username>

# En el archivo escribimos

Minutes Hours DayOfMonth Month Week Script
m h d M w task
* * * * * /root/script.sh -> executes every minute
* 2,4,6 * * * /root/script.sh -> executes at 2,4,6 am
0 9-17 * * * /script.sh -> executes between 9 and 17 hours
10 4,21 */3 * * /scripth.sh -> 10 seconds, between 4,21 hours, every 3 day

@yearly <task>
@monthly <task>
@weekly <task>
@daily <task>
@hourly <task>
@reboot <task> runs every boot time

# Example
*/2 * * * * date >> /tmp/date_and_time.txt

# see in real time cron logs
tail -f /var/log/syslog
```
