# SSH

SSH: Secure Shell

SSH For Services

Jenkins need to connect to another server via SSH

#### Connect to remote server

```sh
ssh <username>@<ipaddress>
```

#### Generate SSH key-pair

```sh
# rsa(cryptographic algo)
ssh-keygen -t rsa
```
#### Copy file to remote server

```sh
scp <filename> <username>@<ipaddress>/<directory>
```

