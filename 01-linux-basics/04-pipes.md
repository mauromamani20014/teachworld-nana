# Pipes and Redirection

## Pipe: the output of the previous command will be the input of the next command

#### less: pagination
```
cat <file> | less
```

#### grep: find
```
cat <file> | grep <search>
```

## Redirection

#### > | >>
```
cat <file> > file.replaced.txt
cat <file> >> file.appended.txt
```

## Standard input std output
```
STDIN(0) = std input
STDOUT(1) = std output
STDERR(2) = std error
```

