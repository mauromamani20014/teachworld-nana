# Environment Variabler

### Print all env variables

```shell
printenv

printenv <env-name>
```

### Create env

```shell
export ENV_EXAMPLE=env_example
```

### Remove env

```shell
unset ENV_EXAMPLE=env_example
```

### Set env variables into .bashrc to make it persistent (.bashrc is user specific)

```shell
export ENV_EXAMPLE=env_example
```
