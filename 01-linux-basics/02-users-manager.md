# Linux Accounts

## 3 User categories

- Superuser Acc
- User Acc
- Service Acc

## How to manage permissions

- User Level: u1, u2, etc
- Group Level: devops, developers, etc

## User Manager Practice 

etc/passdw columns
USERNAME:PASSWORD:UID:GID:GECOS:HOMEDIR:SHELL

## Create a user
```
useradd --flags
```

## Change password
```
passwd <username>
```

## Create group
```
groupadd --flags
```

### Change user group
```
sudo usermod -g <groupname> <username>
```
### Add user to groups (Override previous groups)
```
sudo usermod -G <groupname, groupname2> <username>
```
### Add user to groups
```
sudo usermod -aG <groupname, groupname2> <username>
```
### Delete group
```
sudo delgroup <groupname>
```
### See user's group
```
group <username>
```
### Remove user from group
```
sudo gpasswd -d <username> <group>
```
