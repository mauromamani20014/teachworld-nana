# Introduction to package manager ##

## Apt package manager
```
sudo apt install <name>
```

## Snap package manager
```
sudo snap install <name>
```

## Add repository to APT
/etc/apt/sources.list

### Search package
```
sudo apt search <name>
```

### Diferencia apt | apt-get
```
apt: user friendly, more commands
```
